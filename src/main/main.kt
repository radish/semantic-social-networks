import javafx.scene.input.DataFormat.URL
import org.eclipse.rdf4j.repository.sail.SailRepository;
import org.eclipse.rdf4j.sail.nativerdf.NativeStore;
import java.io.File
import org.eclipse.rdf4j.repository.RepositoryConnection
import org.eclipse.rdf4j.rio.RDFFormat.RDFXML
import org.eclipse.rdf4j.rio.RDFFormat
import org.eclipse.rdf4j.query.QueryLanguage
import org.eclipse.rdf4j.query.TupleQueryResult
import org.eclipse.rdf4j.query.BindingSet
import java.util.List;
import org.eclipse.rdf4j.RDF4JException;
import org.eclipse.rdf4j.query.TupleQuery;

class dummyClass{
    //wat wat wat, nikt tego nie czyta
}
fun main(args: Array<String>) {
    val dataDir = File("/file_dictionary/")
    val repo = SailRepository(NativeStore(dataDir))
    repo.initialize()

    val projectPath: String = dummyClass::class.java!!.getProtectionDomain().getCodeSource().getLocation().toURI().getPath()
    val file = File(projectPath + "/" + "../../../twitter-user-rdfxml.owl")
    val baseURI = "https://gitlab.com/radish/semantic-social-networks/"
    try {
        val con = repo.connection
        try {
            con.add(file, baseURI, RDFFormat.RDFXML)

            val queryString = "SELECT ?x ?y WHERE { ?x ?p ?y } "
            val tupleQuery = con.prepareTupleQuery(QueryLanguage.SPARQL, queryString)
            val result = tupleQuery.evaluate()
            while (result.hasNext()) {  // iterate over the result
                val bindingSet = result.next()
                val valueOfX = bindingSet.getValue("x")
                val valueOfY = bindingSet.getValue("y")
                // do something interesting with the values here...

            }

        } finally {
            con.close()
        }
    } catch (e: RDF4JException) {
        // handle exception
    } catch (e: Exception) {
        // handle io exception
        print("WAT");
    }
    print("Hello")
}
